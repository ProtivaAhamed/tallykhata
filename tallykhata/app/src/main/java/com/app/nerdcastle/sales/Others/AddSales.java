package com.app.nerdcastle.sales.Others;

import java.util.ArrayList;

public class AddSales {

    private ArrayList<StockOutProducts> addSingleSales;

    public AddSales(ArrayList<StockOutProducts> addSingleSales) {
        this.addSingleSales = addSingleSales;
    }

    public ArrayList<StockOutProducts> getAddSingleSales() {
        return addSingleSales;
    }

    public void setAddSingleSales(ArrayList<StockOutProducts> addSingleSales) {
        this.addSingleSales = addSingleSales;
    }
}
