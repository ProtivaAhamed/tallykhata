package com.app.nerdcastle.sales.Others;

public class TotalReport {
    private double TotalInvestment;
    private double TotalIncome ;
    private double TotalProfit ;
    private double TotalDue ;

    public TotalReport(double totalInvestment, double totalIncome, double totalProfit, double totalDue) {
        TotalInvestment = totalInvestment;
        TotalIncome = totalIncome;
        TotalProfit = totalProfit;
        TotalDue = totalDue;
    }

    public double getTotalInvestment() {
        return TotalInvestment;
    }

    public double getTotalIncome() {
        return TotalIncome;
    }

    public double getTotalProfit() {
        return TotalProfit;
    }

    public double getTotalDue() {
        return TotalDue;
    }
}
