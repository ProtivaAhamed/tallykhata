package com.app.nerdcastle.sales.Others;

public class StockInHistoryData {
    private String StockInDateTimes;
    private String StockInQuantitys;
    private String UnitPrices;
    private int ProductId;
    private String ProductName;
    private int ReOrderLevel;
    private int AvailableQuantity;


    public StockInHistoryData(String stockInDateTimes, String stockInQuantitys, String unitPrices, int productId, String productName, int reOrderLevel, int availableQuantity) {
        StockInDateTimes = stockInDateTimes;
        StockInQuantitys = stockInQuantitys;
        UnitPrices = unitPrices;
        ProductId = productId;
        ProductName = productName;
        ReOrderLevel = reOrderLevel;
        AvailableQuantity = availableQuantity;
    }

    public String getStockInDateTimes() {
        return StockInDateTimes;
    }

    public String getStockInQuantitys() {
        return StockInQuantitys;
    }

    public String getUnitPrices() {
        return UnitPrices;
    }

    public int getProductId() {
        return ProductId;
    }

    public String getProductName() {
        return ProductName;
    }

    public int getReOrderLevel() {
        return ReOrderLevel;
    }

    public int getAvailableQuantity() {
        return AvailableQuantity;
    }
}

