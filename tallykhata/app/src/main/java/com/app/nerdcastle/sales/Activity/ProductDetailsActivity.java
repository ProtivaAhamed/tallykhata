package com.app.nerdcastle.sales.Activity;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.Adapter.RecyclearviewAdapterStockInHistoryData;
import com.app.nerdcastle.sales.Interface.OnRefreshListener;
import com.app.nerdcastle.sales.Others.StockInHistoryData;
import com.app.nerdcastle.sales.Others.ProductHistory;
import com.app.nerdcastle.sales.Others.ProductsDetails;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.app.nerdcastle.sales.UI.StockInBottomSheet;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsActivity extends AppCompatActivity  implements OnRefreshListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<StockInHistoryData> stockInHistoryData;
    private RecyclearviewAdapterStockInHistoryData adapter;
    private RetrofitInterface retrofitInterface;
    private ShimmerFrameLayout shimmerFrameLayout;
    private String url;
    private List<ProductsDetails> productDetails;
    private TextView product_name, available_quantity, re_order;
    private ProductsDetails productDetail;
    private FloatingActionButton stockinbtn;
    private int id;
    private ProductHistory productHistory;
    private Button backbtn;


    public ProductDetailsActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

        final String productID = getIntent().getStringExtra("product_id");
        id = Integer.valueOf(productID);

        backbtn=findViewById(R.id.backBtn);
        product_name = findViewById(R.id.productNameShow);
        available_quantity = findViewById(R.id.availableProductQuantity);
        re_order = findViewById(R.id.reorderLevelDetails);
        stockinbtn = findViewById(R.id.addstock);
        recyclerView = findViewById(R.id.recyclearview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        shimmerFrameLayout = findViewById(R.id.shimmar_view);

        backbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        stockinbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StockInBottomSheet stockInBottomSheet = new StockInBottomSheet(ProductDetailsActivity.this);
                Bundle bundle = new Bundle();
                bundle.putString("demo", productID);
                stockInBottomSheet.setArguments(bundle);
                stockInBottomSheet.show(getSupportFragmentManager(), "AddProductBottomSheet");
            }
        });


        allProductDetails();

    }


    public void allProductDetails() {
        url = String.format("/api/ProductListAndDetails/%d", id);
        retrofitInterface = ApiClient.getmInstance().getApi();
        Call<ProductsDetails> call = retrofitInterface.getProductDetails(url);
        call.enqueue(new Callback<ProductsDetails>() {
            @Override
            public void onResponse(Call<ProductsDetails> call, Response<ProductsDetails> response) {
                productDetail = response.body();
                product_name.setText(productDetail.getProductName());
                available_quantity.setText(String.valueOf(productDetail.getAvailableQuantity()));
                re_order.setText(String.valueOf(productDetail.getReOrderLevel()));

                List<ProductHistory> productHistoryList = productDetail.getProductHistory();
                adapter= new RecyclearviewAdapterStockInHistoryData(ProductDetailsActivity.this,productHistoryList);
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(adapter);
                int s=productHistoryList.size();
                if(s==0) {
                    Toast.makeText(ProductDetailsActivity.this, "No Stock History Available", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ProductsDetails> call, Throwable t) {

            }
        });


    }
    @Override
    public void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }


    @Override
    public void onDelete() {
        allProductDetails();
    }
}
