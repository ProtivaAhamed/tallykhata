package com.app.nerdcastle.sales.Others;

public class ProductListData {
    private String ProductName;
    private int AvailableQuantity;
    private int ProductId;

    public ProductListData(String productName, int availableQuantity, int productId) {
        ProductName = productName;
        AvailableQuantity = availableQuantity;
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public int getAvailableQuantity() {
        return AvailableQuantity;
    }

    public int getProductId() {
        return ProductId;
    }
}
