
package com.app.nerdcastle.sales.Others;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductHistory {

    @SerializedName("StockInQuantity")
    @Expose
    private Integer stockInQuantity;
    @SerializedName("StockInPrice")
    @Expose
    private Integer stockInPrice;
    @SerializedName("StockInDate")
    @Expose
    private String stockInDate;

    public Integer getStockInQuantity() {
        return stockInQuantity;
    }

    public void setStockInQuantity(Integer stockInQuantity) {
        this.stockInQuantity = stockInQuantity;
    }

    public Integer getStockInPrice() {
        return stockInPrice;
    }

    public void setStockInPrice(Integer stockInPrice) {
        this.stockInPrice = stockInPrice;
    }

    public String getStockInDate() {
        return stockInDate;
    }

    public void setStockInDate(String stockInDate) {
        this.stockInDate = stockInDate;
    }

}
