package com.app.nerdcastle.sales.Others;

public class StockOutProducts {

    private String ProductName;
    private int StockOutQuantity;
    private int StockOutPrice;
    private int ProductId;

    public StockOutProducts(String productName, int stockOutQuantity, int stockOutPrice, int productId) {
        ProductName = productName;
        StockOutQuantity = stockOutQuantity;
        StockOutPrice = stockOutPrice;
        ProductId = productId;
    }

    public String getProductName() {
        return ProductName;
    }

    public int getStockOutQuantity() {
        return StockOutQuantity;
    }

    public int getStockOutPrice() {
        return StockOutPrice;
    }

    public int getProductId() {
        return ProductId;
    }


    public void setProductName(String productName) {
        ProductName = productName;
    }

    public void setStockOutQuantity(int stockOutQuantity) {
        StockOutQuantity = stockOutQuantity;
    }

    public void setStockOutPrice(int stockOutPrice) {
        StockOutPrice = stockOutPrice;
    }

    public void setProductId(int productId) {
        ProductId = productId;
    }
}
