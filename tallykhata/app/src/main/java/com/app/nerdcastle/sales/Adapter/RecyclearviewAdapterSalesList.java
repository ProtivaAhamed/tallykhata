package com.app.nerdcastle.sales.Adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.nerdcastle.sales.Others.SalesListData;
import com.app.nerdcastle.sales.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclearviewAdapterSalesList extends RecyclerView.Adapter<RecyclearviewAdapterSalesList.SalesViewHolder> {
    private Context salesContext;
    private List<SalesListData> salesList;

    public RecyclearviewAdapterSalesList(Context salesContext, List<SalesListData> salesList) {
        this.salesContext = salesContext;
        this.salesList = salesList;
    }

    @Override
    public RecyclearviewAdapterSalesList.SalesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclearview_sales_list,parent,false);
        SalesViewHolder holder= new SalesViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(RecyclearviewAdapterSalesList.SalesViewHolder holder, int position) {
        SalesListData salesListData= salesList.get(position);

        holder.sold_product_name.setText(salesListData.getProductName());
        holder.sold_product_quantity.setText(String.valueOf(salesListData.getSoldQuantity()));
        holder.available_product_quantity.setText(String.valueOf(salesListData.getAvailableQuantity()));

    }

    @Override
    public int getItemCount() {
        return salesList.size();
    }

    public class SalesViewHolder extends RecyclerView.ViewHolder {
        TextView sold_product_name,sold_product_quantity,available_product_quantity;

        public SalesViewHolder(View view) {
            super(view);
            sold_product_name=view.findViewById(R.id.soldproductname);
            sold_product_quantity=view.findViewById(R.id.soldProductQuantity);
            available_product_quantity=view.findViewById(R.id.salesAvailableProduct);

        }
    }

    public void updateList(List<SalesListData> newList){

        salesList = new ArrayList<>();
        salesList.addAll(newList);
        notifyDataSetChanged();

    }
}
