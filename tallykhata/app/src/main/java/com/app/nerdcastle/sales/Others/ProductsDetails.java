
package com.app.nerdcastle.sales.Others;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductsDetails extends com.app.nerdcastle.sales.Others.ProductHistory {

    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("ImageUrl")
    @Expose
    private Object imageUrl;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("ReOrderLevel")
    @Expose
    private Integer reOrderLevel;
    @SerializedName("AvailableQuantity")
    @Expose
    private Integer availableQuantity;
    @SerializedName("ProductHistory")
    @Expose
    private List<ProductHistory> productHistory = null;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Object getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Object imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getReOrderLevel() {
        return reOrderLevel;
    }

    public void setReOrderLevel(Integer reOrderLevel) {
        this.reOrderLevel = reOrderLevel;
    }

    public Integer getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(Integer availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public List<ProductHistory> getProductHistory() {
        return productHistory;
    }

    public void setProductHistory(List<ProductHistory> productHistory) {
        this.productHistory = productHistory;
    }

}
