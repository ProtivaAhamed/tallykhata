package com.app.nerdcastle.sales.Others;

public class SalesListData {

    private String ProductName;
    private int SoldQuantity;
    private int AvailableQuantity;

    public SalesListData(String productName, int soldQuantity, int availableQuantity) {
        ProductName = productName;
        SoldQuantity = soldQuantity;
        AvailableQuantity = availableQuantity;
    }

    public String getProductName() {
        return ProductName;
    }

    public int getSoldQuantity() {
        return SoldQuantity;
    }

    public int getAvailableQuantity() {
        return AvailableQuantity;
    }
}
