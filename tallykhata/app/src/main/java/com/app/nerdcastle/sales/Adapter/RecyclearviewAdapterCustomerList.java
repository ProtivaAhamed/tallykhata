package com.app.nerdcastle.sales.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.nerdcastle.sales.Activity.CustomersActivity;
import com.app.nerdcastle.sales.Others.CustomerListData;
import com.app.nerdcastle.sales.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Callback;

import static android.content.ContentValues.TAG;

public class RecyclearviewAdapterCustomerList extends RecyclerView.Adapter<RecyclearviewAdapterCustomerList.MyViewHolder> {
    private Context mctx;
    private List<CustomerListData> CustomerList;
    private static final int REQUEST_CALL = 1;

    public RecyclearviewAdapterCustomerList(Context mctx, List<CustomerListData> customerList) {
        this.mctx = mctx;
        this.CustomerList = customerList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclearview_customer_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        final CustomerListData customerListData = CustomerList.get(position);

        holder.customer_name.setText(customerListData.getCustomerName());
        //holder.customer_phone.setText(customerListData.getPhoneNo());
        holder.customer_due.setText(String.valueOf(customerListData.getDueAmount()));
        holder.call_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber= String.valueOf(customerListData.getPhoneNo());
                String dial = "tel:" + phoneNumber;
                if (ActivityCompat.checkSelfPermission(mctx, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                mctx.startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
            }
        });



    }

    @Override
    public int getItemCount() {
        return CustomerList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView customer_name, customer_phone, customer_due;
        ImageButton call_button;

        public MyViewHolder(@NonNull View view) {
            super(view);

            customer_name = view.findViewById(R.id.customerNameR);
            customer_due = view.findViewById(R.id.customerDueR);
            call_button = view.findViewById(R.id.callButton);
        }
    }

    public void updateList(List<CustomerListData> newList){

        CustomerList = new ArrayList<>();
        CustomerList.addAll(newList);
        notifyDataSetChanged();

    }
}
