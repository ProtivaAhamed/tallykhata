package com.app.nerdcastle.sales.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.app.nerdcastle.sales.Activity.DuePaymentActivity;
import com.app.nerdcastle.sales.Others.CustomerListData;
import com.app.nerdcastle.sales.R;

import java.util.ArrayList;
import java.util.List;

public class RecyclearviewAdapterDueCustomerList extends RecyclerView.Adapter<RecyclearviewAdapterDueCustomerList.MyViewHolder> {
    private Context mctx;
    private List<CustomerListData> CustomerList;

    public RecyclearviewAdapterDueCustomerList(Context mctx, List<CustomerListData> customerList) {
        this.mctx = mctx;
        this.CustomerList = customerList;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclearview_due_customer_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {


        final CustomerListData customerListData = CustomerList.get(position);

        holder.customer_name.setText(customerListData.getCustomerName());
        holder.customer_phone.setText(String.valueOf(customerListData.getPhoneNo()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = customerListData.getCustomerName();
                String dueAmount =String.valueOf(customerListData.getDueAmount());
                String customerId=String.valueOf(customerListData.getCustomerId());

                Intent intent = new Intent(mctx, DuePaymentActivity.class);
                intent.putExtra("name", name);
                intent.putExtra("due", dueAmount);
                intent.putExtra("id",customerId);

                mctx.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return CustomerList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView customer_name, customer_phone;

        public MyViewHolder(@NonNull View view) {
            super(view);

            customer_name = view.findViewById(R.id.dueCustomerName);
            customer_phone = view.findViewById(R.id.dueCustomerPhone);

        }
    }

    public void updateList(List<CustomerListData> newList) {

        CustomerList = new ArrayList<>();
        CustomerList.addAll(newList);
        notifyDataSetChanged();

    }
}
