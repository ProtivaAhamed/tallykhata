package com.app.nerdcastle.sales.Fragment;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;

import com.app.nerdcastle.sales.Activity.MainActivity;
import com.app.nerdcastle.sales.Interface.OnRefreshListener;
import com.app.nerdcastle.sales.Others.CustomerListData;
import com.app.nerdcastle.sales.Others.ProductListData;
import com.app.nerdcastle.sales.Adapter.RecyclearviewAdapterProductList;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.app.nerdcastle.sales.UI.AddProductBottomSheet;
import com.app.nerdcastle.sales.R;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsFragment extends Fragment implements OnRefreshListener, SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ProductListData> productListData;
    private RecyclearviewAdapterProductList adapter;
    private RetrofitInterface retrofitInterface;
    private AutoCompleteTextView autoCompleteTextView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton add_product_bottom;
    private boolean isLoaded = false;
    private Toolbar toolbarTB;

    public ProductsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        add_product_bottom = view.findViewById(R.id.addproduct);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        toolbar.setTitle("Products");

        add_product_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddProductBottomSheet addProductBottomSheet = new AddProductBottomSheet(ProductsFragment.this);
                addProductBottomSheet.show(((FragmentActivity) getContext()).getSupportFragmentManager(), "AddProductBottomSheet");

            }
        });
        productListData = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclearview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        shimmerFrameLayout = view.findViewById(R.id.shimmar_view);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        toolbarTB = view.findViewById(R.id.toolbar);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProductList();
            }
        });

        retrofitInterface = ApiClient.getmInstance().getApi();
        getProductList();

        return view;
    }


    private void getProductList() {
        Call<List<ProductListData>> call = retrofitInterface.getProductList();
        call.enqueue(new Callback<List<ProductListData>>() {
            @Override
            public void onResponse(Call<List<ProductListData>> call, Response<List<ProductListData>> response) {
                productListData.clear();
                productListData = response.body();
                adapter = new RecyclearviewAdapterProductList(getActivity(), productListData, ProductsFragment.this);
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                add_product_bottom.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(adapter);
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<List<ProductListData>> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onDelete() {
        getProductList();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        MenuItem menuItem= menu.findItem(R.id.action_search);
        android.support.v7.widget.SearchView searchView= (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String userInput =newText.toLowerCase();
        List<ProductListData> newList= new ArrayList<>();

        for(ProductListData productListData : productListData){

            if(productListData.getProductName().toLowerCase().contains(userInput)){
                newList.add(productListData);
            }
        }
        adapter.updateList(newList);
        return true;
    }
}