package com.app.nerdcastle.sales.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.app.nerdcastle.sales.R;

public class Intro_Page extends AppCompatActivity {
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro__page);
    }

    public void OpenOTP(View view) {
        intent=new Intent(Intro_Page.this,MainActivity.class);
        startActivity(intent);
        finish();

    }
}
