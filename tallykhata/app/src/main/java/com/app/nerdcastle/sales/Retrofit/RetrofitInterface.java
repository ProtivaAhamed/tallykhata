package com.app.nerdcastle.sales.Retrofit;

import com.app.nerdcastle.sales.Others.StockOutProducts;
import com.app.nerdcastle.sales.Others.CustomerListData;
import com.app.nerdcastle.sales.Others.EditProductsData;
import com.app.nerdcastle.sales.Others.ProductListData;
import com.app.nerdcastle.sales.Others.SalesListData;
import com.app.nerdcastle.sales.Others.TotalReport;
import com.app.nerdcastle.sales.Others.ProductsDetails;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface RetrofitInterface {

    @FormUrlEncoded
    @POST("/api/Products")
    Call<ResponseBody> addProduct(
            @Field("ProductName") String ProductName,
            @Field("Description") String Description,
            @Field("SizeOrWeight") String SizeOrWeight,
            @Field("Color") String Color,
            @Field("ReOrderLevel") int ReOrderLevel,
           // @Field("AvailableQuantity") int AvailableQuantity,
            @Field("ProductBarCode") String ProductBarCode
    );

    @GET()
    Call<EditProductsData> editProducts(@Url String url);

    @FormUrlEncoded
    @PUT("/api/Products/{id}")
    Call<ResponseBody> updateProducts(@Path("id") int id,
                                      @Field("ProductName") String ProductName,
                                      @Field("Description") String Description,
                                      @Field("SizeOrWeight") String SizeOrWeight,
                                      @Field("Color") String Color,
                                      @Field("ReOrderLevel") int ReOrderLevel,
                                      // @Field("AvailableQuantity") int AvailableQuantity,
                                      @Field("ProductBarCode") String ProductBarCode
    );

    @GET("/api/ProductListAndDetails")
    Call<List<ProductListData>> getProductList();

    @GET()
    Call<ProductsDetails> getProductDetails(@Url String url);

    @GET()
    Call<List<ProductsDetails>> getProductDetailsList(@Url String url);


    @FormUrlEncoded
    @POST("/api/Customers")
    Call<ResponseBody> addCustomer(
            @Field("PhoneNo") String PhoneNo,
            @Field("CustomerName") String CustomerName,
            @Field("Address") String Address

    );

    @GET("/api/Customers")
    Call<List<CustomerListData>> getCustomerList();


    @FormUrlEncoded
    @POST("/api/StockInProducts")
    Call<ResponseBody> PostStockIn(
            @Field("ProductId") int ProductId,
            @Field("StockInQuantity") int StockInQuantity,
            @Field("StockInPrice") int StockInPrice,
            @Field("StockInDate") String StockInDate

    );

    @GET("/api/SalesList")
    Call<List<SalesListData>> SalesList();

    @DELETE("/api/products/{id}")
    Call<Void> deleteProduct(@Path("id") int id);

    @GET("/api/TotalReport")
    Call<TotalReport> getTotalReport();

    @POST("/api/StockOutProducts")
    Call<ResponseBody> getStockOut(
            @Field("StockOutProducts" ) List<StockOutProducts> StockOutProducts
    );

    @POST("/api/dueSales/")
    Call<ResponseBody> postDueSales(
            @Field("CustomerId") int CustomerId,
            @Field("PayableAmount") int PayableAmount,
            @Field("ProbablePayableDate") String ProbablePayableDate,
            @Field("StockOutProducts" ) List<StockOutProducts> StockOutProducts
    );

}

