package com.app.nerdcastle.sales.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.Activity.ProductDetailsActivity;
import com.app.nerdcastle.sales.Interface.OnRefreshListener;
import com.app.nerdcastle.sales.Others.ProductListData;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.app.nerdcastle.sales.UI.AddProductBottomSheet;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.nerdcastle.sales.R.id.menu_edit_delete;

public class RecyclearviewAdapterProductList extends RecyclerView.Adapter<RecyclearviewAdapterProductList.MyViewHolder> {
    private Context mctx;
    private List<ProductListData> Productlist;
    private RetrofitInterface retrofitInterface;
    private OnRefreshListener onRefreshListener;
    private int pro_id;
    private ProductListData productListData;

    public RecyclearviewAdapterProductList(Context mctx, List<ProductListData> productlist, OnRefreshListener onRefreshListener) {
        this.mctx = mctx;
        this.Productlist = productlist;
        this.onRefreshListener = onRefreshListener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclearview_product_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final ProductListData productListData = Productlist.get(position);


        holder.product_name.setText(productListData.getProductName());
        holder.available_product.setText(String.valueOf(productListData.getAvailableQuantity()));


        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pro_id = productListData.getProductId();

                Intent intent = new Intent(mctx, ProductDetailsActivity.class);
                intent.putExtra("product_id", String.valueOf(pro_id));
                mctx.startActivity(intent);

            }
        });
        holder.edit_delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(mctx, v);

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.item1:
                                int prod_id = productListData.getProductId();
                                AddProductBottomSheet addProductBottomSheet = new AddProductBottomSheet(onRefreshListener);
                                addProductBottomSheet.show(((FragmentActivity) mctx).getSupportFragmentManager(), "AddProductBottomSheet");
                                Bundle bundle = new Bundle();
                                bundle.putInt("demo", prod_id);
                                bundle.putInt("edit", 123);
                                addProductBottomSheet.setArguments(bundle);
                                return true;

                            case R.id.item2:
                                AlertDialog.Builder builder = new AlertDialog.Builder(mctx);
                                builder.setTitle("Delete Product")
                                        .setMessage("Are you sure?")
                                        .setCancelable(false)
                                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                pro_id = productListData.getProductId();
                                                retrofitInterface = ApiClient.getmInstance().getApi();
                                                Call<Void> call = retrofitInterface.deleteProduct(pro_id);
                                                call.enqueue(new Callback<Void>() {
                                                    @Override
                                                    public void onResponse(Call<Void> call, Response<Void> response) {
                                                        Toast.makeText(mctx, "Delete Successfully", Toast.LENGTH_LONG).show();
                                                        onRefreshListener.onDelete();

                                                    }

                                                    @Override
                                                    public void onFailure(Call<Void> call, Throwable t) {
                                                        Toast.makeText(mctx, "Selected Option: No", Toast.LENGTH_SHORT).show();

                                                    }
                                                });


                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {

                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();



                                return true;

                            default:
                                return false;
                        }
                    }

                });
                popup.inflate(R.menu.products_list_popup_menu);


                popup.show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return Productlist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView product_name, available_product, product_Id;
        RelativeLayout parentLayout;
        ImageButton edit_delete_button;

        public MyViewHolder(@NonNull View view) {
            super(view);

            product_name = view.findViewById(R.id.productName);
            available_product = view.findViewById(R.id.avaiableProduct);
            parentLayout = view.findViewById(R.id.parent_layout);
            edit_delete_button = view.findViewById(menu_edit_delete);

        }
    }

    public void updateList(List<ProductListData> newList){

        Productlist = new ArrayList<>();
        Productlist.addAll(newList);
        notifyDataSetChanged();

    }

}
