package com.app.nerdcastle.sales.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.app.nerdcastle.sales.Adapter.RecyclearviewAdapterCustomerList;
import com.app.nerdcastle.sales.Others.CustomerListData;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomersActivity extends AppCompatActivity implements SearchView.OnQueryTextListener{

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<CustomerListData> customerListData;
    private RecyclearviewAdapterCustomerList adapter;
    private RetrofitInterface retrofitInterface;
    private Toolbar toolbar;
    private Button backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customers);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        backBtn=findViewById(R.id.backBtn);
        customerListData = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclearview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        retrofitInterface =  ApiClient.getmInstance().getApi();
        getAllCustomerList();

    }

    private void getAllCustomerList() {
        Call<List<CustomerListData>> call = retrofitInterface.getCustomerList();
        call.enqueue(new Callback<List<CustomerListData>>() {
            @Override
            public void onResponse(Call<List<CustomerListData>> call, Response<List<CustomerListData>> response) {
                customerListData = response.body();
                adapter = new RecyclearviewAdapterCustomerList(CustomersActivity.this,customerListData);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<CustomerListData>> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu,menu);

        MenuItem menuItem= menu.findItem(R.id.action_search);
        SearchView searchView= (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(this);
        return true;

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String userInput =newText.toLowerCase();
        List<CustomerListData> newList= new ArrayList<>();

        for(CustomerListData customerListData : customerListData){

            if(customerListData.getCustomerName().toLowerCase().contains(userInput)){
                newList.add(customerListData);
            }
        }
        adapter.updateList(newList);
        return true;
    }
}
