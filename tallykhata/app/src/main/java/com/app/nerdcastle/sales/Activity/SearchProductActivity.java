package com.app.nerdcastle.sales.Activity;

import android.app.Activity;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AutoCompleteTextView;

import com.app.nerdcastle.sales.Adapter.RecyclearviewAdapterProductList;
import com.app.nerdcastle.sales.Adapter.SearchProductAdapter;
import com.app.nerdcastle.sales.Fragment.ProductsFragment;
import com.app.nerdcastle.sales.Interface.OnRefreshListener;
import com.app.nerdcastle.sales.Others.ProductListData;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchProductActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ProductListData> productList;
    private SearchProductAdapter adapter;
    private RetrofitInterface retrofitInterface;
    private AutoCompleteTextView autoCompleteTextView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FloatingActionButton add_product_bottom;
    private boolean isLoaded = false;
    private Toolbar toolbarTB;
    private SearchView searchView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);
        searchView=findViewById(R.id.searchProduct);
        searchView.setOnQueryTextListener(this);

        productList = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclearview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        retrofitInterface = ApiClient.getmInstance().getApi();
        getProductList();


    }

    private void getProductList() {
        Call<List<ProductListData>> call = retrofitInterface.getProductList();
        call.enqueue(new Callback<List<ProductListData>>() {
            @Override
            public void onResponse(Call<List<ProductListData>> call, Response<List<ProductListData>> response) {
                productList = response.body();
                adapter = new SearchProductAdapter(SearchProductActivity.this,productList);
                recyclerView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<List<ProductListData>> call, Throwable t) {

            }
        });
    }



    public void goBack(View view) {
        onBackPressed();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String userInput =newText.toLowerCase();
        List<ProductListData> newList= new ArrayList<>();

        for(ProductListData productListData : productList){

            if(productListData.getProductName().toLowerCase().contains(userInput)){
                newList.add(productListData);
            }
        }
        adapter.updateList(newList);
        return true;
    }
}
