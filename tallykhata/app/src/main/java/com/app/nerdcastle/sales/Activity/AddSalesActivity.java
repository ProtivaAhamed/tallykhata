package com.app.nerdcastle.sales.Activity;

import android.annotation.SuppressLint;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.Adapter.AddSalesAdapter;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.app.nerdcastle.sales.Others.StockOutProducts;
import com.app.nerdcastle.sales.Interface.OnSalesListener;
import com.app.nerdcastle.sales.R;

import java.util.ArrayList;
import java.util.List;

public class AddSalesActivity extends AppCompatActivity implements OnSalesListener {

    private RetrofitInterface retrofitInterface;
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private AddSalesAdapter adapter;
    private Button due,payment;
    private String p,q;
    private List<StockOutProducts> stockOutProducts;
    private TextView total,addOpen;
    String pname;
    int pquantity,pprice;
    double finalVal=0,val;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sales);

        due=findViewById(R.id.duePayment);
        payment=findViewById(R.id.paidAmount);
        total=findViewById(R.id.totalTV);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        stockOutProducts =new ArrayList<>();
        recyclerView =findViewById(R.id.recyclearviewAddSales);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(AddSalesActivity.this));
        recyclerView.setAdapter(adapter);

        due.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs= PreferenceManager.getDefaultSharedPreferences(AddSalesActivity.this);
                SharedPreferences.Editor editor= prefs.edit();
                editor.putString("total", String.valueOf(finalVal));
                editor.commit();

                Intent intent=new Intent(AddSalesActivity.this,DuePaymentActivity.class);
                //intent.putExtra("list", (Serializable) stockOutProducts);
                startActivity(intent);

            }
        });



        payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AddSalesActivity.this,"Under Maintenance....",Toast.LENGTH_SHORT).show();

                /*retrofitInterface= ApiClient.getmInstance().getApi();
                retrofit2.Call<ResponseBody> call= retrofitInterface.getStockOut(stockOutProducts);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(retrofit2.Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(AddSalesActivity.this,"DONE",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(retrofit2.Call<ResponseBody> call, Throwable t) {

                    }
                });
                */
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.filter_menu, menu);
        return true;
    }


    @SuppressLint("RestrictedApi")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.filterMenuId:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivityForResult(new Intent(AddSalesActivity.this, SalesAddProductActivity.class),100,
                            ActivityOptions.makeSceneTransitionAnimation(this).toBundle());
                }else {
                    startActivityForResult(new Intent(AddSalesActivity.this, SalesAddProductActivity.class),100);
                }


        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                pname=data.getStringExtra("name");
                q=data.getStringExtra("quantity");
                pquantity=Integer.parseInt(q);
                p =data.getStringExtra("price");
                pprice=Integer.parseInt(p);
                finalVal=finalVal+(pquantity*pprice);
                total.setText(String.valueOf(finalVal));
                stockOutProducts.add(new StockOutProducts(pname,pquantity,pprice,2));
                adapter= new AddSalesAdapter(AddSalesActivity.this,stockOutProducts);
                recyclerView.setAdapter(adapter);
            } else {
                Toast.makeText(this, "Add Sales Cancelled", Toast.LENGTH_SHORT).show();

            }
        }
    }


    @Override
    public void salesProduct(String name, String quantity, String price) {
        pname=name;
        q=quantity;
        pquantity=Integer.parseInt(q);
        p =price;
        pprice=Integer.parseInt(p);
        finalVal+=pquantity*pquantity;
        stockOutProducts.add(new StockOutProducts(pname,pquantity,pprice,2));
        adapter= new AddSalesAdapter(AddSalesActivity.this,stockOutProducts);
        recyclerView.setAdapter(adapter);
    }
}
