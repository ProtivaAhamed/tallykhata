package com.app.nerdcastle.sales.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.nerdcastle.sales.Others.ProductHistory;
import com.app.nerdcastle.sales.R;

import java.util.List;

public class RecyclearviewAdapterStockInHistoryData extends RecyclerView.Adapter<RecyclearviewAdapterStockInHistoryData.MyViewHolder> {

    private Context contextStock;
    private List<ProductHistory> productHistoryList;

    public RecyclearviewAdapterStockInHistoryData(Context contextStock, List<ProductHistory> productHistoryList) {
        this.contextStock = contextStock;
        this.productHistoryList = productHistoryList;
    }

    @Override
    public RecyclearviewAdapterStockInHistoryData.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclearview_stock_in_history, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclearviewAdapterStockInHistoryData.MyViewHolder holder, int position) {
        ProductHistory productHistory= productHistoryList.get(position);

        holder.stock_in_date.setText(productHistory.getStockInDate());
        holder.stock_in_quantity.setText(String.valueOf(productHistory.getStockInQuantity()));
        holder.stock_in_price.setText(String.valueOf(productHistory.getStockInPrice()));


    }

    @Override
    public int getItemCount() {
        return productHistoryList.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView stock_in_date, stock_in_quantity,stock_in_price;


        public MyViewHolder(@NonNull View view) {
            super(view);

            stock_in_date=view.findViewById(R.id.stockInDate);
            stock_in_quantity=view.findViewById(R.id.stockInQuantity);
            stock_in_price=view.findViewById(R.id.stockInPrice);


        }
    }
}
