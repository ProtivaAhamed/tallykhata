package com.app.nerdcastle.sales.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.nerdcastle.sales.Activity.CustomersActivity;
import com.app.nerdcastle.sales.Others.TotalReport;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardFragment extends Fragment {

    private TextView due_customer_more,products_more,invest,income,due,profit;
    private RetrofitInterface retrofitInterface;
    private TotalReport totalReport;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard,container,false);

        initialize(view);

        return  view;
    }

    private void initialize(View view) {

        due_customer_more=view.findViewById(R.id.dueCustomerMore);
        products_more= view.findViewById(R.id.productsMore);
        invest=view.findViewById(R.id.investTV);
        income=view.findViewById(R.id.incomeTV);
        due=view.findViewById(R.id.dueTV);
        profit=view.findViewById(R.id.profitTV);

        due_customer_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CustomersActivity.class));
            }
        });

        retrofitInterface = ApiClient.getmInstance().getApi();
        Call<TotalReport> call = retrofitInterface.getTotalReport();
        call.enqueue(new Callback<TotalReport>() {
            @Override
            public void onResponse(Call<TotalReport> call, Response<TotalReport> response) {
                totalReport=response.body();
                invest.setText(String.valueOf(totalReport.getTotalInvestment()));
                income.setText(String.valueOf(totalReport.getTotalIncome()));
                due.setText(String.valueOf(totalReport.getTotalDue()));
                profit.setText(String.valueOf(totalReport.getTotalProfit()));

            }

            @Override
            public void onFailure(Call<TotalReport> call, Throwable t) {

            }
        });





    }


}
