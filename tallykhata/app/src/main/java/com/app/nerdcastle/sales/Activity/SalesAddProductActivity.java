package com.app.nerdcastle.sales.Activity;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.nerdcastle.sales.Interface.OnSalesListener;
import com.app.nerdcastle.sales.R;

public class SalesAddProductActivity extends AppCompatActivity {

    private TextInputEditText productPrice, productQuantity;
    EditText productName;
    private Toolbar toolbar;
    private String pname, pquantity, pprice, name;
    private Button addBT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales_product_add);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Sales Product");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        productName = findViewById(R.id.productNameTIET);
        productQuantity = findViewById(R.id.productQuantityTIET);
        productPrice = findViewById(R.id.productPriceTIET);
        addBT = findViewById(R.id.addBtn);
        /*

        productName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SalesAddProductActivity.this, SearchProductActivity.class));
            }
        });
        name = getIntent().getStringExtra("name");

        productName.setText(name);
        */

        addBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pname = productName.getText().toString().trim();
                pquantity = productQuantity.getText().toString().trim();
                pprice = productPrice.getText().toString().trim();

                Intent intent = new Intent();
                intent.putExtra("name", pname);
                intent.putExtra("quantity", pquantity);
                intent.putExtra("price", pprice);
                setResult(RESULT_OK, intent);
                onBackPressed();
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.done_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.doneMenuId:
                setResult(RESULT_OK, new Intent().putExtra("sortBy", "Sales Done"));
                onBackPressed();

                onBackPressed();
            case android.R.id.home:
                onBackPressed();
        }

        return true;
    }

    public void goBack(View view) {
        setResult(RESULT_CANCELED);
        onBackPressed();
    }
}
