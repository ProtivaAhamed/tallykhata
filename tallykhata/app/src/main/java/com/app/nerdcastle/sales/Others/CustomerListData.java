package com.app.nerdcastle.sales.Others;

public class CustomerListData {
    private String PhoneNo;
    private String CustomerName;
    private double DueAmount;
    private int CustomerId;

    public CustomerListData(String phoneNo, String customerName, double dueAmount, int customerId) {
        PhoneNo = phoneNo;
        CustomerName = customerName;
        DueAmount = dueAmount;
        CustomerId = customerId;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public double getDueAmount() {
        return DueAmount;
    }

    public int getCustomerId() {
        return CustomerId;
    }
}
