package com.app.nerdcastle.sales.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.app.nerdcastle.sales.Others.StockOutProducts;
import com.app.nerdcastle.sales.R;

import java.util.List;

import static android.media.CamcorderProfile.get;

public class AddSalesAdapter extends RecyclerView.Adapter<AddSalesAdapter.SalesViewHolder> {
    private Context context;
    private List<StockOutProducts> stockOutProduct;
    private double productQchange, totalPrice, quantity, unitPrice, price;

    public AddSalesAdapter(Context context, List<StockOutProducts> stockOutProduct) {
        this.context = context;
        this.stockOutProduct = stockOutProduct;

    }


    @Override
    public AddSalesAdapter.SalesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclearview_add_sales_list, parent, false);
        SalesViewHolder holder = new SalesViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final AddSalesAdapter.SalesViewHolder holder, final int position) {

        final StockOutProducts stockOutProducts = stockOutProduct.get(position);

        holder.productName.setText(stockOutProducts.getProductName());
        holder.productQuantity.setText(String.valueOf(stockOutProducts.getStockOutQuantity()));
        quantity = stockOutProducts.getStockOutQuantity();
        unitPrice = stockOutProducts.getStockOutPrice();
        totalPrice = quantity * unitPrice;
        holder.productPrice.setText(String.valueOf(totalPrice));
        productQchange = stockOutProducts.getStockOutQuantity();



        holder.increse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stockOutProducts.setStockOutQuantity( stockOutProducts.getStockOutQuantity() + 1 );
                notifyItemChanged( position );

            }
        });
        holder.decrese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i=stockOutProducts.getStockOutQuantity();
                if(i>=2){
                stockOutProducts.setStockOutQuantity( stockOutProducts.getStockOutQuantity() - 1 );
                notifyItemChanged( position );
                }
            }
        });


        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.deleteItem(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return stockOutProduct.size();
    }

    public class SalesViewHolder extends RecyclerView.ViewHolder {
        TextView productName, productQuantity, productPrice,increse, decrese;
        ImageButton delete;

        public SalesViewHolder(View view) {
            super(view);
            productName = view.findViewById(R.id.productNameTV);
            productQuantity = view.findViewById(R.id.productQuantityTV);
            productPrice = view.findViewById(R.id.productPriceTV);
            increse = view.findViewById(R.id.increseBtn);
            decrese = view.findViewById(R.id.decreseBtn);
            delete=view.findViewById(R.id.deleteSales);
        }

        public void deleteItem(int index) {
            stockOutProduct.remove(index);
            notifyItemRemoved(index);
        }
    }


}
