package com.app.nerdcastle.sales.Activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.app.nerdcastle.sales.Others.StockOutProducts;
import com.app.nerdcastle.sales.UI.CustomerRegistrationBottomSheet;

import java.util.Calendar;
import java.util.List;

public class DuePaymentActivity extends AppCompatActivity {

    private ImageButton addcustomerbtn;
    private String name,due,ctotal,pid,paydate;
    private TextView customerName,previousDue,currentTotal;
    private EditText search,payableDate,payableAmount;
    private DatePickerDialog picker;
    private Button save;
    private RetrofitInterface retrofitInterface;
    private int customerID,payAmout;
    private List<StockOutProducts> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_due_payment);

        name=getIntent().getStringExtra("name");
        due=getIntent().getStringExtra("due");
        pid=getIntent().getStringExtra("id");
        //Intent i = getIntent();
        //list = (List<StockOutProducts>) i.getSerializableExtra("list");


        addcustomerbtn=findViewById(R.id.addCustomer);
        customerName=findViewById(R.id.customerNameTV);
        previousDue=findViewById(R.id.previousDueTV);
        currentTotal=findViewById(R.id.currentTotalTv);
        search=findViewById(R.id.searchET);
        payableAmount=findViewById(R.id.payableAmountET);
        payableDate=findViewById(R.id.payDate);
        save=findViewById(R.id.saveDuePaymentBtn);



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DuePaymentActivity.this,"Under Maintenance....",Toast.LENGTH_SHORT).show();
                /*
                payAmout=Integer.parseInt(payableAmount.getText().toString());
                paydate=payableDate.getText().toString().trim();
                customerID=Integer.parseInt(pid);

                retrofitInterface= ApiClient.getmInstance().getApi();
                Call<ResponseBody> call= retrofitInterface.postDueSales(5,payAmout,paydate,list);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(DuePaymentActivity.this,"Save",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });

                */
            }
        });


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DuePaymentActivity.this,SearchDueCustomerActivity.class));
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        ctotal= prefs.getString("total","no id");

        customerName.setText(name);
        previousDue.setText(due);
        currentTotal.setText(ctotal);


        payableDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(DuePaymentActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                              payableDate.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        addcustomerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomerRegistrationBottomSheet customerRegistrationBottomSheet = new CustomerRegistrationBottomSheet();
                customerRegistrationBottomSheet.show(getSupportFragmentManager(),"customerRegistrationBottomSheet");

            }
        });

    }


}
