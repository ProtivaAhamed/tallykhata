package com.app.nerdcastle.sales.Activity;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.app.nerdcastle.sales.Fragment.DashboardFragment;
import com.app.nerdcastle.sales.Fragment.MoreFragment;
import com.app.nerdcastle.sales.Fragment.ProductsFragment;
import com.app.nerdcastle.sales.Fragment.SalesFragment;
import com.app.nerdcastle.sales.R;

import java.lang.reflect.Field;


public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;

    private DashboardFragment dashboardFragment;
    private ProductsFragment productsFragment;
    private SalesFragment salesFragment;
    private MoreFragment moreFragment;
    private Boolean doubleBackToExitPressedOnce = false;


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.nav_dashboard:
                    replaceFragment(dashboardFragment);
                    return true;
                case R.id.nav_products:
                    replaceFragment(productsFragment);
                    return true;
                case R.id.nav_sales:
                    replaceFragment(salesFragment);
                    return true;
                case R.id.nav_more:
                    replaceFragment(moreFragment);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isConnection(MainActivity.this))
        {
            buildDialog(MainActivity.this).show();
        }









        initialization();

        replaceFragment(dashboardFragment);

        BottomNavigationView navigation = findViewById(R.id.bottom_navigation);

        BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


   public boolean isConnection(Context context)
   {
       ConnectivityManager cm= (ConnectivityManager)context.getSystemService(context.CONNECTIVITY_SERVICE);
       NetworkInfo networkInfo= cm.getActiveNetworkInfo();
       if(networkInfo!=null && networkInfo.isConnectedOrConnecting())
       {
           android.net.NetworkInfo wifi=cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
           android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
           if((mobile!= null && mobile.isConnectedOrConnecting()) ||(wifi!=null && wifi.isConnectedOrConnecting()) )
           {
               //Toast.makeText(MainActivity.this,"Network Ok",Toast.LENGTH_LONG).show();
               return true;
           }

       }
       Toast.makeText(MainActivity.this,"Network Not ok!!!",Toast.LENGTH_LONG).show();


     return false;
   }
   public AlertDialog.Builder buildDialog(Context c)
   {
       AlertDialog.Builder builder=new AlertDialog.Builder(c);
       builder.setTitle("No Internet Connection");
       builder.setMessage("You need to have internet connection to run this app");
       builder.setPositiveButton("exit", new DialogInterface.OnClickListener() {
           @Override
           public void onClick(DialogInterface dialog, int which) {
               finish();
           }
       });
       return builder;
   }


    private void initialization() {
        dashboardFragment = new DashboardFragment();
        productsFragment = new ProductsFragment();
        salesFragment = new SalesFragment();
        moreFragment = new MoreFragment();
    }


    private void replaceFragment(Fragment fragment) {

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_container, fragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            this.finish();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please press again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }


}

