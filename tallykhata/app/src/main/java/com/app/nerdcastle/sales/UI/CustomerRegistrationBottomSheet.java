package com.app.nerdcastle.sales.UI;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerRegistrationBottomSheet extends BottomSheetDialogFragment {

    TextInputEditText phone,name,address;
    String phone_number,customer_name,customer_address;
    Button savebutton;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.bottom_sheet_customer_registration,container,false);

        initialize(view);

        return view;

    }

    private void initialize(View view) {

        phone=view.findViewById(R.id.phoneNo);
        name=view.findViewById(R.id.customerName);
        address=view.findViewById(R.id.customerAddress);
        savebutton=view.findViewById(R.id.save);

        savebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                phone_number = phone.getText().toString().trim();
                customer_name = name.getText().toString().trim();
                customer_address=address.getText().toString().trim();

                if (phone_number.isEmpty()) {
                    phone.setError("Please Enter Phone No");
                    phone.requestFocus();
                    return;
                }

                if (customer_name.isEmpty()) {
                    name.setError("Please Enter Customer Name");
                    address.requestFocus();
                    return;
                }

                Call<ResponseBody> call = ApiClient
                        .getmInstance()
                        .getApi()
                        .addCustomer(phone_number,customer_name,customer_address);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        try {
                            String s = response.body().string();
                            Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                            dismiss();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            }
        });






    }
}
