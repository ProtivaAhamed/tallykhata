package com.app.nerdcastle.sales.Others;

public class EditProductsData {
    private String ProductName;
    private String BarCodeNo;
    private int ReOrderLevel;
    private String SizeOrWeight;
    private String Color;
    private String Description;
    private String ImageUrl ;
    private int AvailableQuantity;

    public EditProductsData(String productName, String barCodeNo, int reOrderLevel, String sizeOrWeight, String color, String description, String imageUrl, int availableQuantity) {
        ProductName = productName;
        BarCodeNo = barCodeNo;
        ReOrderLevel = reOrderLevel;
        SizeOrWeight = sizeOrWeight;
        Color = color;
        Description = description;
        ImageUrl = imageUrl;
        AvailableQuantity = availableQuantity;
    }

    public String getProductName() {
        return ProductName;
    }

    public String getBarCodeNo() {
        return BarCodeNo;
    }

    public int getReOrderLevel() {
        return ReOrderLevel;
    }

    public String getSizeOrWeight() {
        return SizeOrWeight;
    }

    public String getColor() {
        return Color;
    }

    public String getDescription() {
        return Description;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public int getAvailableQuantity() {
        return AvailableQuantity;
    }
}
