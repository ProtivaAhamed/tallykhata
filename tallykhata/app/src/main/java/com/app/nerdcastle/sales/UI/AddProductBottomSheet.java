package com.app.nerdcastle.sales.UI;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.Adapter.RecyclearviewAdapterProductList;
import com.app.nerdcastle.sales.Interface.OnRefreshListener;
import com.app.nerdcastle.sales.Others.EditProductsData;
import com.app.nerdcastle.sales.Others.ProductListData;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Others.ScanBarcode;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;


import java.io.IOException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddProductBottomSheet extends BottomSheetDialogFragment {

    private TextInputEditText product_name, barcode_no, reorder_level, size_weight, color_product,stockOutNotify;
    private String product, barcode, reorder, sizeweightproduct, colorproduct;
    private int reod,notify;
    private RetrofitInterface retrofitInterface;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<ProductListData> productListData;
    private RecyclearviewAdapterProductList adapter;
    private EditProductsData editProductsData;
    private OnRefreshListener onRefreshListener;
    private Button save_data;
    private TextView title_;
    private ImageButton cambtn, gallerybtn, bar_code;
    private Integer REQUEST_CAMERA = 1, SELECT_FILE = 0;
    private CircleImageView add_image_product;
    private int edit_open, id_product, add_open;
    private String url;




    public AddProductBottomSheet()
    {

    }


    @SuppressLint("ValidFragment")
    public AddProductBottomSheet(OnRefreshListener onRefreshListener){
        this.onRefreshListener=onRefreshListener;
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_add_product, container, false);

        save_data = view.findViewById(R.id.save);
        add_image_product = view.findViewById(R.id.add_image);
        stockOutNotify=view.findViewById(R.id.reOrderstockNotify);
        product_name = view.findViewById(R.id.productname);
        barcode_no = view.findViewById(R.id.barcodeno);
        size_weight = view.findViewById(R.id.sizeweight);
        color_product = view.findViewById(R.id.colorcode);
        bar_code = view.findViewById(R.id.barCode);
        title_ = view.findViewById(R.id.title_add);


        //**********************EDIT DATA*****************//

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            id_product = bundle.getInt("demo");
            edit_open = bundle.getInt("edit");
            add_open = bundle.getInt("add");
        }
        if (edit_open == 123) {
            title_.setText("Edit produtcs");
            save_data.setText("Update");
            url = String.format("/api/Products/%d", id_product);
            retrofitInterface = ApiClient.getmInstance().getApi();
            Call<EditProductsData> call = retrofitInterface.editProducts(url);
            call.enqueue(new Callback<EditProductsData>() {
                @Override
                public void onResponse(Call<EditProductsData> call, Response<EditProductsData> response) {
                    editProductsData = response.body();
                    product_name.setText(editProductsData.getProductName());
                    barcode_no.setText(editProductsData.getBarCodeNo());
                    stockOutNotify.setText(String.valueOf(editProductsData.getReOrderLevel()));
                    size_weight.setText(editProductsData.getSizeOrWeight());
                    color_product.setText(editProductsData.getColor());

                }

                @Override
                public void onFailure(Call<EditProductsData> call, Throwable t) {

                }
            });

            save_data.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    product = product_name.getText().toString().trim();
                    barcode = barcode_no.getText().toString().trim();
                    notify=Integer.parseInt(stockOutNotify.getText().toString());
                    sizeweightproduct = size_weight.getText().toString().trim();
                    colorproduct = color_product.getText().toString().trim();
                    retrofitInterface=ApiClient.getmInstance().getApi();
                    Call<ResponseBody> call= retrofitInterface.updateProducts(
                            id_product,product,"",sizeweightproduct,colorproduct,notify,barcode);

                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            String s = null;
                            try {
                                s = response.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            onRefreshListener.onDelete();
                            Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                            dismiss();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });
                }


            });

        }


        //*********************************************//

        else {
            save_data.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        notify = Integer.parseInt(stockOutNotify.getText().toString());
                        product = product_name.getText().toString().trim();
                        barcode = barcode_no.getText().toString().trim();
                        sizeweightproduct = size_weight.getText().toString().trim();
                        colorproduct = color_product.getText().toString().trim();


                        Call<ResponseBody> call = ApiClient
                                .getmInstance()
                                .getApi()
                                .addProduct(product, "",
                                        sizeweightproduct, colorproduct, notify, barcode);

                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                String s = null;
                                try {
                                    s = response.body().string();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                onRefreshListener.onDelete();
                                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                                dismiss();

                            }


                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
                    } catch(NumberFormatException ex){
                        Toast.makeText(getActivity(), "Please Fill the Form", Toast.LENGTH_LONG).show();
                    }

                }

            });
        }


        bar_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ScanBarcode.class);
                startActivityForResult(intent, 0);

            }
        });


        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_box_add_product_image);
        dialog.setTitle("Title...");


        add_image_product.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.show();

                cambtn = dialog.findViewById(R.id.opencamera);
                gallerybtn = dialog.findViewById(R.id.oepngallery);

                cambtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, REQUEST_CAMERA);


                    }
                });

                gallerybtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(intent.createChooser(intent, "Select File"), SELECT_FILE);

                    }
                });

            }
        });


        //   initialize(view);

        return view;

    }

   /* private void initialize(View view) {



    }*/


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                Bundle bundle = data.getExtras();
                final Bitmap bmp = (Bitmap) bundle.get("data");
                add_image_product.setImageBitmap(bmp);
            } else if (requestCode == SELECT_FILE) {
                Uri selectImageuri = data.getData();
                add_image_product.setImageURI(selectImageuri);
            }
        }
        if (requestCode == 0) {


            if (requestCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    Barcode barcode = data.getParcelableExtra("barcode");
                    barcode_no.setText(barcode.displayValue);
                } else {
                    barcode_no.setText("No barcode found");
                }
            }

        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }


    }

}