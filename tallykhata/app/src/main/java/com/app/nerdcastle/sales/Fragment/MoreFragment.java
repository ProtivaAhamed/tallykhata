package com.app.nerdcastle.sales.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.app.nerdcastle.sales.Activity.CustomersActivity;
import com.app.nerdcastle.sales.R;

public class MoreFragment extends Fragment {

    LinearLayout customerbtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
         View view=inflater.inflate(R.layout.fragment_more,container,false);

         initialize(view);

         return view;
    }

    private void initialize(View view) {

        customerbtn=view.findViewById(R.id.customers);

        customerbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CustomersActivity.class));
            }
        });
    }
}
