package com.app.nerdcastle.sales.UI;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.TextInputEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.Interface.OnRefreshListener;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockInBottomSheet extends BottomSheetDialogFragment {

    EditText datePickBtn;
    final Calendar myCalendar = Calendar.getInstance();
    private TextInputEditText stock_quatity, stock_price, product_id;
    private Button save_Button;
    private int price, quantity, product_id_details;
    private TextView date_time;
    private String i;
    private DatePickerDialog picker;
    private String sendate;
    private OnRefreshListener onRefreshListener;

    public StockInBottomSheet() {

    }

    @SuppressLint("ValidFragment")
    public StockInBottomSheet(OnRefreshListener onRefreshListener) {
        this.onRefreshListener = onRefreshListener;

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bottom_sheet_stock_in, container, false);

        initialize(view);
        return view;
    }

    private void initialize(View view) {
        stock_price = view.findViewById(R.id.stockPrice);
        stock_quatity = view.findViewById(R.id.stockQuantity);
        datePickBtn = view.findViewById(R.id.salesDates);
        save_Button = view.findViewById(R.id.saveStock);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            i = bundle.getString("demo");
        }

        datePickBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                datePickBtn.setText(year + "/" + (monthOfYear + 1) + "/" + dayOfMonth);
                            }
                        }, year, month, day);
                picker.show();
            }
        });


        save_Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try{
                    price = Integer.parseInt(stock_price.getText().toString());
                    quantity = Integer.parseInt(stock_quatity.getText().toString());
                    sendate = datePickBtn.getText().toString();
                    product_id_details = Integer.parseInt(i);

                    Call<ResponseBody> call = ApiClient
                            .getmInstance()
                            .getApi()
                            .PostStockIn(product_id_details, quantity, price, sendate);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            try {
                                String s = response.body().string();
                                Toast.makeText(getActivity(), s, Toast.LENGTH_LONG).show();
                                onRefreshListener.onDelete();
                                dismiss();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });

                }catch(NumberFormatException ex){
                    Toast.makeText(getActivity(), "Please Fill the Form", Toast.LENGTH_LONG).show();
                }


                }


        });
    }
}
