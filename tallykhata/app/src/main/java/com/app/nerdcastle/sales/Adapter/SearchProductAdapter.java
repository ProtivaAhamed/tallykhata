package com.app.nerdcastle.sales.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.nerdcastle.sales.Activity.ProductDetailsActivity;
import com.app.nerdcastle.sales.Activity.SalesAddProductActivity;
import com.app.nerdcastle.sales.Activity.SearchProductActivity;
import com.app.nerdcastle.sales.Interface.OnRefreshListener;
import com.app.nerdcastle.sales.Others.ProductListData;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.app.nerdcastle.sales.UI.AddProductBottomSheet;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.nerdcastle.sales.R.id.menu_edit_delete;

public class SearchProductAdapter extends RecyclerView.Adapter<SearchProductAdapter.MyViewHolder> {
    private Context mctx;
    private List<ProductListData> Productlist;
    private int pro_id;

    public SearchProductAdapter(Context mctx, List<ProductListData> productlist) {
        this.mctx = mctx;
        Productlist = productlist;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclearview_search_product, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        final ProductListData productListData = Productlist.get(position);

        holder.product_name.setText(productListData.getProductName());
        holder.available_product.setText(String.valueOf(productListData.getAvailableQuantity()));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pro_id = productListData.getProductId();
                String name= productListData.getProductName();
                int quantity =productListData.getAvailableQuantity();

                Intent intent = new Intent(mctx, SalesAddProductActivity.class);
                intent.putExtra("product_id", String.valueOf(pro_id));
                intent.putExtra("name",name);
                intent.putExtra("quantity",String.valueOf(quantity));
                mctx.startActivity(intent);
                ((Activity)mctx).finish();

            }
        });

    }


    @Override
    public int getItemCount() {
        return Productlist.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView product_name, available_product;


        public MyViewHolder(@NonNull View view) {
            super(view);

            product_name = view.findViewById(R.id.productNameTV);
            available_product = view.findViewById(R.id.availableQuantityTV);
        }
    }

    public void updateList(List<ProductListData> newList){

        Productlist = new ArrayList<>();
        Productlist.addAll(newList);
        notifyDataSetChanged();

    }

}
