package com.app.nerdcastle.sales.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;

import com.app.nerdcastle.sales.Activity.AddSalesActivity;
import com.app.nerdcastle.sales.Adapter.RecyclearviewAdapterSalesList;
import com.app.nerdcastle.sales.Others.SalesListData;
import com.app.nerdcastle.sales.R;
import com.app.nerdcastle.sales.Retrofit.ApiClient;
import com.app.nerdcastle.sales.Retrofit.RetrofitInterface;
import com.facebook.shimmer.ShimmerFrameLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SalesFragment extends Fragment implements SearchView.OnQueryTextListener{

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<SalesListData> salesListData;
    private RecyclearviewAdapterSalesList adapter;
    private RetrofitInterface retrofitInterface;
    private ShimmerFrameLayout shimmerFrameLayout;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_sales, container, false);

        initialize(view);

        return view;
    }

    private void initialize(View view) {

        FloatingActionButton floatingActionButton = view.findViewById(R.id.addsales);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AddSalesActivity.class);
                startActivity(intent);

            }
        });
        shimmerFrameLayout = view.findViewById(R.id.shimmar_view);
        salesListData = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclearview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        retrofitInterface = ApiClient.getmInstance().getApi();
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRecentSalesList();
            }
        });

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);


        getRecentSalesList();
    }

    private void getRecentSalesList() {

        Call<List<SalesListData>> call = retrofitInterface.SalesList();
        call.enqueue(new Callback<List<SalesListData>>() {
            @Override
            public void onResponse(Call<List<SalesListData>> call, Response<List<SalesListData>> response) {
                salesListData = response.body();
                adapter = new RecyclearviewAdapterSalesList(getActivity(), salesListData);
                shimmerFrameLayout.stopShimmer();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.setAdapter(adapter);
                swipeRefreshLayout.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<List<SalesListData>> call, Throwable t) {

            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        shimmerFrameLayout.startShimmer();
    }

    @Override
    public void onPause() {
        super.onPause();
        shimmerFrameLayout.stopShimmer();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        getActivity().getMenuInflater().inflate(R.menu.toolbar_menu,menu);
        MenuItem menuItem= menu.findItem(R.id.action_search);
        android.support.v7.widget.SearchView searchView= (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(SalesFragment.this);

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String userInput =newText.toLowerCase();
        List<SalesListData> newList= new ArrayList<>();

        for(SalesListData salesListData : salesListData){

            if(salesListData.getProductName().toLowerCase().contains(userInput)){
                newList.add(salesListData);
            }
        }
        adapter.updateList(newList);
        return true;
    }
}